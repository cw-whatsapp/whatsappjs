const router = require('express').Router();
const { MessageMedia, Location } = require("whatsapp-web.js");
const request = require('request')
const vuri = require('valid-url');
const fs = require('fs');
const shell = require('shelljs');
const childProcess = require("child_process");

const gs = require('ghostscript4js');




const mediadownloader = (url, path, callback) => {
    request.head(url, (err, res, body) => {
        request(url)
            .pipe(fs.createWriteStream(path))
            .on('close', callback)
    })
}
router.get("/getqr", async (req, res) => {
    client
        .getState()
        .then((data) => {
            if (data) {

                res.status(200).send({ status: 'success', qr: `Ya autenticada` });

            } else sendQr(res);
        })
        .catch(() => sendQr(res));
});

function sendQr(res) {
    fs.readFile("components/last.qr", (err, last_qr) => {
        if (!err && last_qr) {

            res.status(200).send({ status: 'success', qr: `${last_qr}` });
        }
    });
}

router.get("/checkauth", async (req, res) => {
    client
        .getState()
        .then((data) => {
            var info = client.info;
            res.status(200).send({ status: 'success', message: data, info: { name: info.pushname, number: info.wid.user } });
        })
        .catch((err) => {
            if (err) {
                res.status(200).send({ status: 'error', message: `DISCONNECTED`, info: { name: "", number: "" } });
            }
        });
});

router.post('/sendPdfMini', async (req, res) => {

    let pdfBase64 = req.body.pdf;
    let pdfName = req.body.docName;

    if (pdfBase64 == undefined || pdfName == undefined) {
        res.send({ status: "error", message: "Se requiere base64 de pdf" })
    } else {

        try {
            // Generar el pdf como File
            await fs.writeFile("./data/file/" + pdfName, pdfBase64, { encoding: 'base64' },
                function (err) {
                    if (err) {
                        console.log('err', err);
                        res.send({ status: "error", message: err })
                    } else {
                        //Comprimir pdf
                        console.log('ok', "pdf generado... inicio comprime");
                        childProcess.exec(`gswin64c -sDEVICE=pdfwrite -dCompatibilityLevel=20  -dNOPAUSE -dQUIET -dBATCH -sOutputFile=./data/filemini/${pdfName} ./data/file/${pdfName}`,
                            (error, stdout, stderr) => {

                                if (error) {
                                    res.send({ status: "error", message: error.message });
                                }
                                res.send({ status: "success", message: pdfName + "  minificado" });
                            }
                        );
                    }
                }
            );

        } catch (e) {
            res.send({ status: "error", message: "error inesperado" });
        }

    }
});

router.post('/sendpdf', async (req, res) => {
    let id = req.body.id;
    let pdfName = req.body.pdfName;
    let pdfName2 = req.body.pdfName2;

    if (pdfName == undefined || id == undefined || pdfName2 == undefined) {
        res.send({ status: "error", message: "Faltan datos del Pdf." })
    } else {

        try {
            console.log("enviando pdf-:" + pdfName);
            var body = await fs.readFileSync('./data/filemini/' + pdfName2);
            let media = await new MessageMedia('application/pdf', body.toString('base64'), pdfName);
            await client.sendMessage(id, media, { caption: "pdf" }).then((response) => {
                if (response.id.fromMe) {

                    res.send({ status: 'success', message: `Message enviado. ${pdfName}.` });
                    // fs.unlinkSync(path)
                }
            });

        } catch (e) {
            res.send({ status: "error", message: e });
        }

    }
});

router.post('/sendimg', async (req, res) => {

    let id = req.body.id;
    let img = req.body.img;



    if (img == undefined || id == undefined) {
        res.send({ status: "error", message: "Se requiere base64" })
    } else {
        try {


            if (!fs.existsSync('./temp')) {
                fs.mkdirSync('./temp');
            }

            let media = new MessageMedia('image/png', img);
            client.sendMessage(id, media, { caption: "" }).then((response) => {
                if (response.id.fromMe) {

                    res.send({ status: 'success', message: `Message enviado.` });
                    // fs.unlinkSync(path)

                }
            });
        } catch (e) {
            res.send({ status: "error", message: e.toString() })
        }
    }
});

router.post('/sendmessage', async (req, res) => {
    let phone = req.body.phone;
    let message = req.body.message;

    if (phone == undefined || message == undefined) {
        res.send({ status: "error", message: "Se requiere id, message." })
    } else {
        client.sendMessage(phone + '@c.us', message).then((response) => {
            if (response.id.fromMe) {
                res.send({ status: 'success', message: `Message enviado a ${phone}` })
            }
        });
    }
});

router.post('/sendmessagegroup', async (req, res) => {
    let id = req.body.id;
    let message = req.body.message;

    if (id == undefined || message == undefined) {
        res.send({ status: "error", message: "Se requiere id, message." });
    } else {
        try {
            client.sendMessage(id, message).then((response) => {
                if (response.id.fromMe) {
                    res.send({ status: 'success', message: `mensaje enviado a ${id}` });
                }
            });
        } catch (e) {
            res.send({ status: "error", message: e.toString() });
        }

    }
});

router.post('/clearmessage', async (req, res) => {
    let id = req.body.id;


    client.getChatById(id).then((chat) => {
        console.log("Chat information = ", chat)
        chat.clearMessages().then((deleteRes) => {
            if (deleteRes) {

                res.send({ status: 'success', message: `mensajes limpiados.` });
            }
            else {

                res.send({ status: 'error', message: `mensajes no limpiados` });
            }
        })
    }).catch((err) => {
        if (err.message.includes("Cannot read property 'serialize' of undefined"))
            res.send({ status: 'error', message: `mensajes no limpiados` });
        // can handle other error messages...     
    })
});


router.post('/getfiltersorlist', async (req, res) => {

    let filtro = req.body.filtro;


    client.getChats().then((chats) => {


        var listArray = [];
        const myGroup = chats.forEach(element => {
            if (element.isGroup == true && element.groupMetadata != undefined) {

                if (filtro.length != 0 || filtro != '') {
                    var dataStringMa = filtro.toUpperCase();
                    var dataStringMi = filtro.toLowerCase();
                    var reg = new RegExp("^(" + dataStringMa + "|" + dataStringMi + "|" + filtro + ").*$");

                    if (element.name.match(reg)) {
                        listArray.push({
                            id: element.id._serialized,
                            name: element.name,
                            create: element.groupMetadata.creation,
                            owner: element.groupMetadata.owner,
                            description: (element.groupMetadata.desc == undefined) ? "-" : element.groupMetadata.desc,
                            participants: element.groupMetadata.participants,
                            status: 0
                        });
                    }
                } else {
                    listArray.push({
                        id: element.id._serialized,
                        name: element.name,
                        create: element.groupMetadata.creation,
                        owner: element.groupMetadata.owner,
                        description: (element.groupMetadata.desc == undefined) ? "-" : element.groupMetadata.desc,
                        participants: element.groupMetadata.participants,
                        status: 0
                    });
                }
            }
        });

        res.send({ status: 'correcto', lista: listArray });


    });

});


module.exports = router;






