const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const axios = require("axios");
const shelljs = require("shelljs");
const puppeteer = require('puppeteer');
const cors = require("cors");
const config = require("./config.json");
var qrcode = require('qrcode-terminal');
const { Client, LocalAuth } = require("whatsapp-web.js");


process.title = "whatsapp-node-api";
puppeteer.launch({
    headless: true, slowMo: 100, args: [
        '--no-sandbox',
        '--disable-dev-shm-usage', // <-- add this one
    ]
});

global.client = new Client({
    authStrategy: new LocalAuth(),
    puppeteer: {
        args: [
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--no-first-run',
            '--no-zygote',
            '--single-process', // <- this one doesn't works in Windows
            '--disable-gpu',
            '--no-zygote',
            '--shm-size=3gb',
            '--disable-setuid-sandbox',
            '--no-sandbox'],
    }
});

global.authed = false;

const app = express();


const port = process.env.PORT || config.port;

app.use(bodyParser.json({ limit: '900000mb' }));
app.use(bodyParser.urlencoded({ limit: '900000mb', extended: true, }));



app.use(bodyParser.text({ limit: '900000mb' }));

app.use(cors());
// app.use(express.json({
//     verify: (req, res, buf) => {
//         req.rawBody = buf.toString()
//     },
//     limit: "900000mb"
// }));


client.on("qr", (qr) => {
    console.log("new qr : " + qr);
    qrcode.generate(qr, { small: true });
    fs.writeFileSync("./components/last.qr", qr);
});

client.on("authenticated", () => {
    console.log("AUTH!");
    authed = true;

    try {
        fs.unlinkSync("./components/last.qr");
    } catch (err) { }
});

client.on("auth_failure", () => {
    console.log("AUTH Failed !");
    process.exit();
});

client.on("ready", () => {
    console.log("Client is ready!");
});

client.on("message", async (msg) => {

     console.log(msg.body);
    // if (config.webhook.enabled) {
    //     if (msg.hasMedia) {
    //         const attachmentData = await msg.downloadMedia();
    //         msg.attachmentData = attachmentData;
    //     }
    //     axios.post(config.webhook.path, { msg });
    // }
});
client.on("disconnected", () => {
    console.log("disconnected");
});
client.initialize().catch(_ => _);


const groupRoute = require("./components/service_whatsapp");


app.use(function (req, res, next) {
    console.log(req.method + " : " + req.path);
    next();
});

app.use("/group", groupRoute);


app.get("/", (req, res, next) => {
    return res.json({ message: "whatsapp API Server Running" });
});

app.get("/qr", (req, res, next) => {
    if (authed) {
        return res.sendFile("./components/last.qr");
    } else {
        return res.send("Not Authenticated");
    }
});

app.listen(port, () => {
    console.log("Server Running Live on Port : " + port);
});
